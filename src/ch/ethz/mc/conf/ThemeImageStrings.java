package ch.ethz.mc.conf;

/*
 * © 2013-2017 Center for Digital Health Interventions, Health-IS Lab a joint
 * initiative of the Institute of Technology Management at University of St.
 * Gallen and the Department of Management, Technology and Economics at ETH
 * Zurich
 * 
 * For details see README.md file in the root folder of this project.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Contains all images used in the source code to assign images to UI elements
 *
 * @author Andreas Filler
 */
public class ThemeImageStrings {
	// PATH
	private static final String	IMAGE_PATH				= "img/";
	// IMAGES
	public static final String	WELCOME_ICON			= IMAGE_PATH
																+ "welcome-icon.png";
	public static final String	INTERVENTIONS_ICON		= IMAGE_PATH
																+ "interventions-icon.png";
	public static final String	ACCESS_CONTROL_ICON		= IMAGE_PATH
																+ "access-control-icon.png";
	public static final String	ACCOUNT_ICON			= IMAGE_PATH
																+ "account-icon.png";
	public static final String	COMPONENT_ICON			= IMAGE_PATH
																+ "component-icon.png";
	public static final String	BLANK_MEDIA_OBJECT		= IMAGE_PATH
																+ "blank-media-object.png";
	public static final String	ACTIVE_ICON_SMALL		= IMAGE_PATH
																+ "active-icon-small.png";
	public static final String	INACTIVE_ICON_SMALL		= IMAGE_PATH
																+ "inactive-icon-small.png";

	public static final String	MESSAGE_ICON_SMALL		= IMAGE_PATH
																+ "message-icon-small.png";
	public static final String	SUPERVISOR_ICON_SMALL	= IMAGE_PATH
																+ "supervisor-icon-small.png";
	public static final String	RULE_ICON_SMALL			= IMAGE_PATH
																+ "rule-icon-small.png";
	public static final String	STOP_ICON_SMALL			= IMAGE_PATH
																+ "stop-icon-small.png";
}
